-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Mag 31, 2021 alle 16:35
-- Versione del server: 10.4.17-MariaDB
-- Versione PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `cognome` varchar(40) NOT NULL,
  `abbonamento` int(3) NOT NULL,
  `nickname` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `main`
--

CREATE TABLE `main` (
  `ID` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `cognome` varchar(40) NOT NULL,
  `abbonamento` int(2) NOT NULL COMMENT '0 = free\r\n1 = pro\r\n2 = admin ',
  `nickname` varchar(16) NOT NULL,
  `email` varchar(124) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `numero_telefono` varchar(10) DEFAULT NULL,
  `ran_name-pass` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `main`
--

INSERT INTO `main` (`ID`, `nome`, `cognome`, `abbonamento`, `nickname`, `email`, `password`, `numero_telefono`, `ran_name-pass`) VALUES
(1, 'aaa', 'aaa', 0, 'darkaaa', 'aaa@gmail.com', 'aaaaaa', '3333333333', ''),
(2, 'bbb', 'bbb', 1, 'blackbbb', 'bbb@gmail.com', 'bbbbbb', '4444444444', ''),
(3, 'ccc', 'ccc', 2, 'lastccc', 'ccc@gmail.com', 'ccc1234', '1111111111', ''),
(4, 'ddd', 'ddd', 1, '_-magicddd-_', 'ddd@gmail.com', 'ddd1234', '2222222222', ''),
(5, 'giorgio', 'modeo', 0, 'gggg', 'giorgio.modeo@gmail.com', '123456789', '1234567890', '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indici per le tabelle `main`
--
ALTER TABLE `main`
  ADD PRIMARY KEY (`ID`,`nome`) USING BTREE,
  ADD KEY `ran_name-pass` (`ran_name-pass`) USING BTREE,
  ADD KEY `nome` (`nome`),
  ADD KEY `abbonamento` (`abbonamento`),
  ADD KEY `nickname` (`nickname`),
  ADD KEY `numero_telefono` (`numero_telefono`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `main`
--
ALTER TABLE `main`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
