var mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "test"
});
con.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  // Uncomment below lines for first time to create a table in database
  // var sql = "CREATE TABLE users (email VARCHAR(255), password VARCHAR(255))";
  // connection.query(sql, function (err, result) {
  //   if (err) throw err;
  //   console.log("Table created");
  // });
  console.log('connected as id ' + con.threadId);
});
exports.register = async function(req,res){
  const password = req.body.password;
  const numero = req.body.numero_telefono;

  const encryptedPassword = await bcrypt.hash(password, saltRounds)
  const encryptedNumero = await bcrypt.hash(numero, saltRounds)

  var users={
    "name":req.body.name,
    "surname":req.body.surname,
    "abbonamento":req.body.,
    "nickname":req.body.nickname,
    "e-mail":req.body.email,
    "password":encryptedPassword,
    "numero_telefono":req.body.

     "email":req.body.email,
   }

  con.query('INSERT INTO users SET ?',users, function (error, results, fields) {
    if (error) {
      res.send({
        "code":400,
        "failed":"error ocurred"
      })
    } else {
      res.send({
        "code":200,
        "success":"user registered sucessfully"
          });
      }
  });
}

exports.login = async function(req,res){
  var email= req.body.email;
  var password = req.body.password;
  con.query('SELECT * FROM users WHERE email = ?',[email], async function (error, results, fields) {
    if (error) {
      res.send({
        "code":400,
        "failed":"error ocurred"
      })
    }else{
      if(results.length >0){
        const comparision = await bcrypt.compare(password, results[0].password)
        if(comparision){
            res.send({
              "code":200,
              "success":"login sucessfull"
            })
        }
        else{
          res.send({
               "code":204,
               "success":"Email and password does not match"
          })
        }
      }
      else{
        res.send({
          "code":206,
          "success":"Email does not exits"
            });
      }
    }
    });
}
