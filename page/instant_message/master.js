// genera una stringa randomica
function generateRandomString(iLen) {
  var sChrs = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz-_";
  for (var i = 0; i < iLen; i++) {
    var randomPoz = Math.floor(Math.random() * sChrs.length);
    sRnd += sChrs.substring(randomPoz, randomPoz + 1);
  }
  return sRnd;
}

var rs = generateRandomString(8)
[hash: base64: 6]
getElementById('myList').setAttribute('class', rs);

//******************************************************************************
// codice per nascondere un elemento con javascript
// modificare il document.getElementById("mettere l'id del oggetto da nascondere")
  function myFunction(x) {
    if (x.matches) {
      document.getElementById('myList').setAttribute('class', 'Hide');
    } else {
      document.getElementById('myList').setAttribute('class', 'Show');
    }
  }
  var x = window.matchMedia("(max-width: 1024px)")
  myFunction(x)
  x.addListener(myFunction)
//******************************************************************************
