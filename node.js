var express = require("express");
var app = express();
var path = require('path');
var mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;
// prova il funzionamento
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));

// crea un collegamento statico alla cartella playscript master
// quindi da ora in poi i file esterni dovranno essere aggiunti da questa directory
app.use(express.static(__dirname + '/'));

// caricamento della pagina iniziale
app.get("/", function(req, res) {res.sendFile(path.join(__dirname + '/index.html'));});

// *************** log_in ***************
  app.post("/log_in", function(req,res){
    console.log("Ricevuto una richiesta POST");
    // stampa a server console tutto il contenuto del form
    console.log(req.body);
  });

// *************** Sing_up ***************
  app.post("/sing_up", function(req,res){
    console.log("Ricevuto una richiesta POST");
    // contenuto della richiesta
    console.log(req.body);
    res.sendFile(path.join(__dirname + '/index.html'))


    // connesione al db
    var con = mysql.createConnection({
      host: "127.0.0.1",
      user: "root",
      password: "",
      database: "test"
    });
    console.log("Connected!");

    con.connect(function(err) {
      if(err) throw err;
      console.log('connected as id ' + con.threadId);
      // creo la query e al posto delle credenziali singole uso un array in json
      // res.write(JSON.stringify(data));
      const sql = "INSERT INTO main (nome, cognome, nickname, email, password)  VALUES  (?, ?, ?, ?, ?)";
      console.log("assegniola query");
      con.query(sql,[users], function (error, results, fields) {
        if (err) {
          res.end({
            "code":400,
            "failed":"error ocurred",
          })
          console.log("error ocurred");
        } else {
          res.end({
            "code":200,
            "success":"user registered sucessfully",
              });
              console.log("user registered sucessfully");
          }
      });

      // creo l'array con le credenziali
      const pass = req.body.pass;
      const numero = req.body.numero_telefono;
      const encryptedPassword = bcrypt.hash(pass, saltRounds)
      const encryptedNumber = bcrypt.hash(numero, saltRounds)
      var users={
        "nome":"req.body.name",
        "cognome":"req.body.surname",
        "nickname":"req.body.nickname",
        "email":"req.body.email",
        "password":"encryptedPassword",
      }
    });
  });


  app.post("/home", function(req,res){res.sendFile(path.join(__dirname + '/index.html'))});
  app.post("/Chat", function(req,res){res.sendFile(path.join(__dirname + '/page/instant_message/chat_main.html'))});
  app.post("/Gaming_Portal", function(req,res){res.sendFile(path.join(__dirname + '/page/game_page/game_src.html'))});
  app.post("/gioco_2", function(req,res){res.sendFile(path.join(__dirname + '/page/game_page/jenkins/index.html'))});
  app.post("/sing_up_btn", function(req,res){res.sendFile(path.join(__dirname + '/page/Sing_Up/sing_up.html'))});

  app.listen(5002, function() {
    console.log("Listening on 127.0.0.1:5002");
  });
